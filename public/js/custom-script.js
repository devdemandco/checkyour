/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */

var chooseCheck = function chooseCheck (event) {
  var element = event.target;
  var $element = $(event.target);
  var element_id = element.id;
  var $store = $('#check');
  var yes = 'survey-yes';
  var no = 'survey-no';
  var hyes = "#"+yes
  var hno = "#"+no
  var $date = $('#date-row');
  var $survey = $("#survey");

  $(hno).removeClass('lighten-5');
  $(hno).removeClass('darken-4');
  $(hyes).removeClass('lighten-5');
  $(hyes).removeClass('darken-4');

  switch(element_id) {
    case yes:
      $(hno).addClass('lighten-5');
      $store.val("1");
      break;

    case no:
      $(hyes).addClass('lighten-5');
      $store.val("0");
      $date.hide();
      break;
  }

  $element.addClass('darken-4');
  $survey.fadeIn();
};


$(function () {
  $("#survey-form").validate({
    rules: {
        "dob-year": {
            required: true,
            minlength:4,
            digits: true,
            maxlength: 4
        },
        uglies: {
          required: true
        },
        date: {
          required: '#check[value="1"]',
          date: true
        },
        country: {
          required: true
        },
        postcode: {
          required: true,
          digits:true,
          minlength: 4,
          maxlength: 6
        },
      },
      messages: {
        date: {
          required: "You forgot to let us know when your last check was!"
        },
        postcode: {
          required: "Please let us know your post code."
        }
      },
      errorElement : 'div',
      errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
          $(placement).append(error)
        } else {
          error.appendTo(element.parent());
        }
      }
    });

    $('#survey-yes').on('click', chooseCheck);
    $('#survey-no').on('click', chooseCheck);
    $("#survey").hide();
});
