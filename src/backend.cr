require "rethinkdb"
require "kemal"
require "./helpers"
include RethinkDB::Shortcuts

conn = r.connect(host: "db.checkyouruglies.internal", db: "checkyoursurveys")

module Uglies
  get "/" do |env|
    view(env, "public/index.html")
  end

  post "/survey" do |env|
    # Get the form elements and validate
    # Check for date_required
    begin
      date_required = env.params.body["check"]
    rescue KeyError
      next "Missing 'Check' Parameter"
    end

    # Validate date field if required
    date = nil
    if date_required == "1"
      if env.params.body["date"] == ""
        next "Date is required"
      end

      # Try parsing the date
      begin
        date = Time.parse(env.params.body["date"], "%e %B, %Y")
      rescue Time::Format::Error
        next "Date is invalid"
      end
    end

    # Validate the year of birth
    begin
      yob = env.params.body["dob-year"]
    rescue KeyError
      next "Year of birth is required"
    end

    if yob.size != 4
      next "Year of birth is invalid"
    end

    # Validate country
    begin
      country = env.params.body["country"]
    rescue KeyError
      next "Country is required"
    end

    if country.size != 2
      next "Country must be a two character country code"
    end

    if !/[A-Z][A-Z]/i.match country
      next "Country must be a valid country code"
    end

    # Validate postcode
    begin
      postcode = env.params.body["postcode"].to_i
    rescue KeyError
      next "Postcode is required"
    rescue ArgumentError
      next "Postcode must be an integer"
    end

    if postcode > 999999 || postcode < 1000
      next "Postcode must be between 4 and 6 characters"
    end

    # Store in the DB
    result = r.table("surveys").insert({
      :had_check => date_required,
      :year_of_birth => yob,
      :last_check => date,
      :country => country,
      :postcode => postcode
    }).run(conn)

    begin
      if result["inserted"].as_i < 1
        next "Failed to save survey result [1]"
      end
    rescue KeyError
      next "Failed to save survey result [2]"
    end


    # Redirect to the share page
    env.redirect "/thankyou?check=" + date_required

  end
end

get "/thankyou" do |env|
  render "views/thankyou.ecr"
end

Kemal.run()
