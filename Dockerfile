FROM debian:jessie
RUN which curl || (apt-get update && apt-get install -y curl)
RUN ([ -f /usr/lib/apt/methods/https ] && echo "apt-get https exists") || (apt-get update && apt-get install -y apt-transport-https)
RUN which crystal || (curl https://dist.crystal-lang.org/apt/setup.sh | bash && apt-get update && apt-get install -y crystal)
RUN which git || (apt-get update && apt-get install -y git)
RUN which pkg-config || (apt-get update && apt-get install -y pkg-config)
RUN which make || (apt-get update && apt-get install -y gcc make)
RUN apt-get update && apt-get install -y libssl-dev
RUN mkdir /var/opt/site-build && mkdir /var/opt/site
COPY ./src/ /var/opt/site-build/
COPY public /var/opt/site/public
WORKDIR /var/opt/site-build
ENV APP_BIN backend
RUN crystal deps install && crystal build $APP_BIN.cr --release && mv $APP_BIN ../site/
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/opt/site-build/*
WORKDIR /var/opt/site
EXPOSE 3000
CMD ./$APP_BIN
